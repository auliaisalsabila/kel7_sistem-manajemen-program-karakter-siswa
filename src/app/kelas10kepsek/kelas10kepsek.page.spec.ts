import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Kelas10kepsekPage } from './kelas10kepsek.page';

describe('Kelas10kepsekPage', () => {
  let component: Kelas10kepsekPage;
  let fixture: ComponentFixture<Kelas10kepsekPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Kelas10kepsekPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Kelas10kepsekPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
