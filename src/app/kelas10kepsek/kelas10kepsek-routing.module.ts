import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Kelas10kepsekPage } from './kelas10kepsek.page';

const routes: Routes = [
  {
    path: '',
    component: Kelas10kepsekPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Kelas10kepsekPageRoutingModule {}
