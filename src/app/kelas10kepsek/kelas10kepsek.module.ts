import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Kelas10kepsekPageRoutingModule } from './kelas10kepsek-routing.module';

import { Kelas10kepsekPage } from './kelas10kepsek.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Kelas10kepsekPageRoutingModule
  ],
  declarations: [Kelas10kepsekPage]
})
export class Kelas10kepsekPageModule {}
