import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import {Router} from '@angular/router';
import { Menuwalas1Page } from '../menuwalas1/menuwalas1.page';
import { LogoutPage } from '../logout/logout.page';

@Component({
  selector: 'app-uploadkelas11',
  templateUrl: './uploadkelas11.page.html',
  styleUrls: ['./uploadkelas11.page.scss'],
})
export class Uploadkelas11Page implements OnInit {

  constructor(
    public modalController : ModalController,
    public router: Router,
  ) { }

  ngOnInit() {
  }

  async menuwalas1()
  {
    const modal = await this.modalController.create({
      component: Menuwalas1Page,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async logout()
  {
    const modal = await this.modalController.create({
      component: LogoutPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
