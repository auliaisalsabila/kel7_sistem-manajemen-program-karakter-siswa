import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Uploadkelas11PageRoutingModule } from './uploadkelas11-routing.module';

import { Uploadkelas11Page } from './uploadkelas11.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Uploadkelas11PageRoutingModule
  ],
  declarations: [Uploadkelas11Page]
})
export class Uploadkelas11PageModule {}
