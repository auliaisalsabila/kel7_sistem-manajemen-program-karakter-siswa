import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Uploadkelas11Page } from './uploadkelas11.page';

describe('Uploadkelas11Page', () => {
  let component: Uploadkelas11Page;
  let fixture: ComponentFixture<Uploadkelas11Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Uploadkelas11Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Uploadkelas11Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
