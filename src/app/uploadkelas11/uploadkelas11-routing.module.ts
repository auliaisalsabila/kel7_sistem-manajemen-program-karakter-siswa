import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Uploadkelas11Page } from './uploadkelas11.page';

const routes: Routes = [
  {
    path: '',
    component: Uploadkelas11Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Uploadkelas11PageRoutingModule {}
