import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { HalamanAwalPage } from '../halaman-awal/halaman-awal.page';
import {Router} from '@angular/router';
import { AlertController } from '@ionic/angular';

import firebase from '@firebase/app';
import '@firebase/auth';

@Component({
  selector: 'app-loginbk',
  templateUrl: './loginbk.page.html',
  styleUrls: ['./loginbk.page.scss'],
})
export class LoginbkPage implements OnInit {

  email: string = "";
  password: string = "";

  constructor(
    public modalController : ModalController,
    public router: Router,
    public alert: AlertController,
  ) { }

  ngOnInit() {
  }

  selectedSegment:any = 'home';

  async halamanawal()
  {
    const modal = await this.modalController.create({
      component: HalamanAwalPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async loginbk()
  {
    firebase.auth().signInWithEmailAndPassword(this.email, this.password).then((data) =>
    {
      this.modalController.dismiss();
      this.router.navigate(['menubk1']);
    }).catch((err)=>{
      this.showAlert("Login Gagal", err.message)
      console.log("Login Gagal")
    })
  }
  async showAlert(header:string, message:string){
    const alert = await this.alert.create({
      header,
      message,
      buttons: ["OK"]
    })
    await alert.present()
  }
}
