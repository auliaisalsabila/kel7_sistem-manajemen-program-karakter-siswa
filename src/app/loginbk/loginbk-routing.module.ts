import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginbkPage } from './loginbk.page';

const routes: Routes = [
  {
    path: '',
    component: LoginbkPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginbkPageRoutingModule {}
