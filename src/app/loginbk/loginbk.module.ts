import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginbkPageRoutingModule } from './loginbk-routing.module';

import { LoginbkPage } from './loginbk.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoginbkPageRoutingModule
  ],
  declarations: [LoginbkPage]
})
export class LoginbkPageModule {}
