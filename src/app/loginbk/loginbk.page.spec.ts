import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LoginbkPage } from './loginbk.page';

describe('LoginbkPage', () => {
  let component: LoginbkPage;
  let fixture: ComponentFixture<LoginbkPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginbkPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LoginbkPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
