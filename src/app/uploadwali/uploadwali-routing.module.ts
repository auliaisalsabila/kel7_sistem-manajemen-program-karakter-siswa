import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UploadwaliPage } from './uploadwali.page';

const routes: Routes = [
  {
    path: '',
    component: UploadwaliPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UploadwaliPageRoutingModule {}
