import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UploadwaliPage } from './uploadwali.page';

describe('UploadwaliPage', () => {
  let component: UploadwaliPage;
  let fixture: ComponentFixture<UploadwaliPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadwaliPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UploadwaliPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
