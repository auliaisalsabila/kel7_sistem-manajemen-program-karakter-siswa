import { Component, OnInit } from '@angular/core';
import { MenuwalasPage } from '../menuwalas/menuwalas.page';
import { ModalController } from '@ionic/angular';
import {Router} from '@angular/router';


@Component({
  selector: 'app-notifwalas',
  templateUrl: './notifwalas.page.html',
  styleUrls: ['./notifwalas.page.scss'],
})
export class NotifwalasPage implements OnInit {

  constructor(
    public modalController : ModalController,
    public router: Router,
  ) { }

  ngOnInit() {
  }
  
  async menuwalas()
  {
    const modal = await this.modalController.create({
      component: MenuwalasPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

}
