import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotifwalasPage } from './notifwalas.page';

const routes: Routes = [
  {
    path: '',
    component: NotifwalasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotifwalasPageRoutingModule {}
