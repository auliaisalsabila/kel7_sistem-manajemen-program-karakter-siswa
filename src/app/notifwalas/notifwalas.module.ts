import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotifwalasPageRoutingModule } from './notifwalas-routing.module';

import { NotifwalasPage } from './notifwalas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotifwalasPageRoutingModule
  ],
  declarations: [NotifwalasPage]
})
export class NotifwalasPageModule {}
