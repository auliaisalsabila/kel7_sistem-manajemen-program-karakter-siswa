import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NotifwalasPage } from './notifwalas.page';

describe('NotifwalasPage', () => {
  let component: NotifwalasPage;
  let fixture: ComponentFixture<NotifwalasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotifwalasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NotifwalasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
