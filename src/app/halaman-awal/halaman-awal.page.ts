import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { HomePage } from '../home/home.page';
import { LoginPage } from '../login/login.page';
import { LoginbkPage } from '../loginbk/loginbk.page';
import { LoginsiswaPage } from '../loginsiswa/loginsiswa.page';
import { LoginwalasPage } from '../loginwalas/loginwalas.page';
import { LoginwaliPage } from '../loginwali/loginwali.page';

@Component({
  selector: 'app-halaman-awal',
  templateUrl: './halaman-awal.page.html',
  styleUrls: ['./halaman-awal.page.scss'],
})
export class HalamanAwalPage implements OnInit {

  constructor(
    public modalController : ModalController,
    private router: Router
  ) { }

  ngOnInit() {
  }
  selectedSegment:any = 'home';

  async home()
  {
    const modal = await this.modalController.create({
      component: HomePage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async login()
  {
    const modal = await this.modalController.create({
      component: LoginPage,
      cssClass: 'my-custom-class'
    });
    /*modal.onDidDismiss().then(res => {
      console.log(res)
      if(res.data == 'ok') {
        this.modalController.dismiss();
        this.router.navigate(['menukepsek1']);
      }
    }) */
    return await modal.present();
  }
  async loginbk()
  {
    const modal = await this.modalController.create({
      component: LoginbkPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async loginwalas()
  {
    const modal = await this.modalController.create({
      component: LoginwalasPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async loginsiswa()
  {
    const modal = await this.modalController.create({
      component: LoginsiswaPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async loginwali()
  {
    const modal = await this.modalController.create({
      component: LoginwaliPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}

