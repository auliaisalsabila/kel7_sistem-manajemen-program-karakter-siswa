import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HalamanAwalPage } from './halaman-awal.page';

const routes: Routes = [
  {
    path: '',
    component: HalamanAwalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HalamanAwalPageRoutingModule {}
