import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HalamanAwalPageRoutingModule } from './halaman-awal-routing.module';

import { HalamanAwalPage } from './halaman-awal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HalamanAwalPageRoutingModule
  ],
  declarations: [HalamanAwalPage]
})
export class HalamanAwalPageModule {}
