import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HalamanAwalPage } from './halaman-awal.page';

describe('HalamanAwalPage', () => {
  let component: HalamanAwalPage;
  let fixture: ComponentFixture<HalamanAwalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HalamanAwalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HalamanAwalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
