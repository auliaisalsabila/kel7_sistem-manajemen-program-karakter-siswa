import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Kelas11bkPageRoutingModule } from './kelas11bk-routing.module';

import { Kelas11bkPage } from './kelas11bk.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Kelas11bkPageRoutingModule
  ],
  declarations: [Kelas11bkPage]
})
export class Kelas11bkPageModule {}
