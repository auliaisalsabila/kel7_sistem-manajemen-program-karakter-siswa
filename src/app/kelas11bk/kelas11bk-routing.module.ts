import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Kelas11bkPage } from './kelas11bk.page';

const routes: Routes = [
  {
    path: '',
    component: Kelas11bkPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Kelas11bkPageRoutingModule {}
