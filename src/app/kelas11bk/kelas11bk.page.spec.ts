import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Kelas11bkPage } from './kelas11bk.page';

describe('Kelas11bkPage', () => {
  let component: Kelas11bkPage;
  let fixture: ComponentFixture<Kelas11bkPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Kelas11bkPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Kelas11bkPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
