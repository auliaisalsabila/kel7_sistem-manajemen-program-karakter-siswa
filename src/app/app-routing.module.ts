import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'halaman-awal',
    loadChildren: () => import('./halaman-awal/halaman-awal.module').then( m => m.HalamanAwalPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'daftar',
    loadChildren: () => import('./daftar/daftar.module').then( m => m.DaftarPageModule)
  },
  {
    path: 'loginbk',
    loadChildren: () => import('./loginbk/loginbk.module').then( m => m.LoginbkPageModule)
  },
  {
    path: 'loginwalas',
    loadChildren: () => import('./loginwalas/loginwalas.module').then( m => m.LoginwalasPageModule)
  },
  {
    path: 'loginsiswa',
    loadChildren: () => import('./loginsiswa/loginsiswa.module').then( m => m.LoginsiswaPageModule)
  },
  {
    path: 'loginwali',
    loadChildren: () => import('./loginwali/loginwali.module').then( m => m.LoginwaliPageModule)
  },
  {
    path: 'menusiswa',
    loadChildren: () => import('./menusiswa/menusiswa.module').then( m => m.MenusiswaPageModule)
  },
  {
    path: 'menuwalas',
    loadChildren: () => import('./menuwalas/menuwalas.module').then( m => m.MenuwalasPageModule)
  },
  {
    path: 'menuwali',
    loadChildren: () => import('./menuwali/menuwali.module').then( m => m.MenuwaliPageModule)
  },
  {
    path: 'menubk1',
    loadChildren: () => import('./menubk1/menubk1.module').then( m => m.Menubk1PageModule)
  },
  {
    path: 'menukepsek1',
    loadChildren: () => import('./menukepsek1/menukepsek1.module').then( m => m.Menukepsek1PageModule)
  },
  {
    path: 'logout',
    loadChildren: () => import('./logout/logout.module').then( m => m.LogoutPageModule)
  },
  {
    path: 'menuwalas1',
    loadChildren: () => import('./menuwalas1/menuwalas1.module').then( m => m.Menuwalas1PageModule)
  },
  {
    path: 'kelas10kepsek',
    loadChildren: () => import('./kelas10kepsek/kelas10kepsek.module').then( m => m.Kelas10kepsekPageModule)
  },
  {
    path: 'kelas11kepsek',
    loadChildren: () => import('./kelas11kepsek/kelas11kepsek.module').then( m => m.Kelas11kepsekPageModule)
  },
  {
    path: 'kelas12kepsek',
    loadChildren: () => import('./kelas12kepsek/kelas12kepsek.module').then( m => m.Kelas12kepsekPageModule)
  },
  {
    path: 'kelas10bk',
    loadChildren: () => import('./kelas10bk/kelas10bk.module').then( m => m.Kelas10bkPageModule)
  },
  {
    path: 'kelas11bk',
    loadChildren: () => import('./kelas11bk/kelas11bk.module').then( m => m.Kelas11bkPageModule)
  },
  {
    path: 'kelas12bk',
    loadChildren: () => import('./kelas12bk/kelas12bk.module').then( m => m.Kelas12bkPageModule)
  },
  {
    path: 'uploadbk',
    loadChildren: () => import('./uploadbk/uploadbk.module').then( m => m.UploadbkPageModule)
  },
  {
    path: 'uploadkepsek',
    loadChildren: () => import('./uploadkepsek/uploadkepsek.module').then( m => m.UploadkepsekPageModule)
  },
  {
    path: 'uploadwali',
    loadChildren: () => import('./uploadwali/uploadwali.module').then( m => m.UploadwaliPageModule)
  },
  {
    path: 'uploadkelas10',
    loadChildren: () => import('./uploadkelas10/uploadkelas10.module').then( m => m.Uploadkelas10PageModule)
  },
  {
    path: 'uploadkelas11',
    loadChildren: () => import('./uploadkelas11/uploadkelas11.module').then( m => m.Uploadkelas11PageModule)
  },
  {
    path: 'uploadkelas12',
    loadChildren: () => import('./uploadkelas12/uploadkelas12.module').then( m => m.Uploadkelas12PageModule)
  },
  {
    path: 'notifwalas',
    loadChildren: () => import('./notifwalas/notifwalas.module').then( m => m.NotifwalasPageModule)
  },
  {
    path: 'notifsiswa',
    loadChildren: () => import('./notifsiswa/notifsiswa.module').then( m => m.NotifsiswaPageModule)
  },



];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
