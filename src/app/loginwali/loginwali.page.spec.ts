import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LoginwaliPage } from './loginwali.page';

describe('LoginwaliPage', () => {
  let component: LoginwaliPage;
  let fixture: ComponentFixture<LoginwaliPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginwaliPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LoginwaliPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
