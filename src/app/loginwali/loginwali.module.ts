import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginwaliPageRoutingModule } from './loginwali-routing.module';

import { LoginwaliPage } from './loginwali.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoginwaliPageRoutingModule
  ],
  declarations: [LoginwaliPage]
})
export class LoginwaliPageModule {}
