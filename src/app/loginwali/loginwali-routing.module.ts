import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginwaliPage } from './loginwali.page';

const routes: Routes = [
  {
    path: '',
    component: LoginwaliPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginwaliPageRoutingModule {}
