import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Menukepsek1PageRoutingModule } from './menukepsek1-routing.module';

import { Menukepsek1Page } from './menukepsek1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Menukepsek1PageRoutingModule
  ],
  declarations: [Menukepsek1Page]
})
export class Menukepsek1PageModule {}
