import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Menukepsek1Page } from './menukepsek1.page';

const routes: Routes = [
  {
    path: '',
    component: Menukepsek1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Menukepsek1PageRoutingModule {}
