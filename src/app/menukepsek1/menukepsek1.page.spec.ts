import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Menukepsek1Page } from './menukepsek1.page';

describe('Menukepsek1Page', () => {
  let component: Menukepsek1Page;
  let fixture: ComponentFixture<Menukepsek1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menukepsek1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Menukepsek1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
