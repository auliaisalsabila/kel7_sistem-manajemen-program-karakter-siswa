import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import {Router} from '@angular/router';
import { LoginPage } from '../login/login.page';
import { Kelas10kepsekPage } from '../kelas10kepsek/kelas10kepsek.page';
import { Kelas11kepsekPage } from '../kelas11kepsek/kelas11kepsek.page';
import { Kelas12kepsekPage } from '../kelas12kepsek/kelas12kepsek.page';


@Component({
  selector: 'app-menukepsek1',
  templateUrl: './menukepsek1.page.html',
  styleUrls: ['./menukepsek1.page.scss'],
})
export class Menukepsek1Page implements OnInit {

  constructor(
    public modalController : ModalController,
    public router: Router,
  ) { }

  ngOnInit() {
  }

  async login()
  {
    const modal = await this.modalController.create({
      component: LoginPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async kelas10kepsek()
  {
    const modal = await this.modalController.create({
      component: Kelas10kepsekPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async kelas11kepsek()
  {
    const modal = await this.modalController.create({
      component: Kelas11kepsekPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async kelas12kepsek()
  {
    const modal = await this.modalController.create({
      component: Kelas12kepsekPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
