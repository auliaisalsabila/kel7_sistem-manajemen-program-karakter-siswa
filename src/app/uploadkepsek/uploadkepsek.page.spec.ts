import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UploadkepsekPage } from './uploadkepsek.page';

describe('UploadkepsekPage', () => {
  let component: UploadkepsekPage;
  let fixture: ComponentFixture<UploadkepsekPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadkepsekPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UploadkepsekPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
