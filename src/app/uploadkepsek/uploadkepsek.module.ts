import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UploadkepsekPageRoutingModule } from './uploadkepsek-routing.module';

import { UploadkepsekPage } from './uploadkepsek.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UploadkepsekPageRoutingModule
  ],
  declarations: [UploadkepsekPage]
})
export class UploadkepsekPageModule {}
