import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UploadkepsekPage } from './uploadkepsek.page';

const routes: Routes = [
  {
    path: '',
    component: UploadkepsekPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UploadkepsekPageRoutingModule {}
