import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import {Router} from '@angular/router';
import { LoginsiswaPage } from '../loginsiswa/loginsiswa.page';

import { ImageCroppedEvent } from 'ngx-image-cropper';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-menusiswa',
  templateUrl: './menusiswa.page.html',
  styleUrls: ['./menusiswa.page.scss'],
})
export class MenusiswaPage implements OnInit {

  @Input() imageData: any;
  @Input() imagePath: any;
  @Input() ratio: any;
  @Input() width: any;
  @Input() height: any;
  maintainRatio: boolean = false;
  constructor(
    public modalController : ModalController,
    public router: Router,
    public storage: AngularFireStorage
  ) { }

  ngOnInit() {
    if(this.ratio == undefined){
      this.ratio = 0;
    }else{
      this.maintainRatio = true;
    }
    if(this.width == undefined) this.width = 0
    if(this.height = undefined) this.height = 0;
  }

  url: any;
  mulai(){
    this.url='https://www.16personalities.com/id/tes-kepribadian';
    window.open(this.url,'_system');
  }
  async loginsiswa()
  {
    const modal = await this.modalController.create({
      component: LoginsiswaPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  //cropper
  croppedImage: any = '';    
  imageCropped(event: ImageCroppedEvent) {
      this.croppedImage = event.base64;
  } 

  //end cropper
  loading: boolean;
  upload()
  {
    if(this.imagePath != false)
    {
      this.loading = true;
      var ref = this.storage.ref(this.imagePath);   
      ref.putString(this.croppedImage,'data_url').then(res=>{
        this.getUrl();
      }).catch(err=>{
        this.loading = false;
        this.dismiss();
      });
    }else{
      this.modalController.dismiss({
        'imageUrl': this.croppedImage
      });
    }
     
  }

  getUrl()
  {
    this.storage.ref(this.imagePath).getDownloadURL().subscribe(url=>{
      this.loading = false;
      this.modalController.dismiss({
        'imageUrl': url
      });
    },err=>{
      this.loading = false;
    })
  }

  dismiss() {    
    this.modalController.dismiss({
      'imageUrl': false
    });
  }
}