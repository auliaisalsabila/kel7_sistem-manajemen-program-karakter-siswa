import { Component, OnInit } from '@angular/core';
import { LoginwaliPage } from '../loginwali/loginwali.page';
import { ModalController } from '@ionic/angular';
import {Router} from '@angular/router';
import { LogoutPage } from '../logout/logout.page';

@Component({
  selector: 'app-menuwali',
  templateUrl: './menuwali.page.html',
  styleUrls: ['./menuwali.page.scss'],
})
export class MenuwaliPage implements OnInit {

  constructor(
    public modalController : ModalController,
    public router: Router,
  ) { }

  ngOnInit() {
  }

  async loginwali()
  {
    const modal = await this.modalController.create({
      component: LoginwaliPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async logout()
  {
    const modal = await this.modalController.create({
      component: LogoutPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
