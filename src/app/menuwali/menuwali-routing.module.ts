import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuwaliPage } from './menuwali.page';

const routes: Routes = [
  {
    path: '',
    component: MenuwaliPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuwaliPageRoutingModule {}
