import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenuwaliPageRoutingModule } from './menuwali-routing.module';

import { MenuwaliPage } from './menuwali.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenuwaliPageRoutingModule
  ],
  declarations: [MenuwaliPage]
})
export class MenuwaliPageModule {}
