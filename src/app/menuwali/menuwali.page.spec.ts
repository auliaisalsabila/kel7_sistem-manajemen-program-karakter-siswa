import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MenuwaliPage } from './menuwali.page';

describe('MenuwaliPage', () => {
  let component: MenuwaliPage;
  let fixture: ComponentFixture<MenuwaliPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuwaliPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MenuwaliPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
