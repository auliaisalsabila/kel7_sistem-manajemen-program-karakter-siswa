import { ModalController } from '@ionic/angular';
import {Router} from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import firebase from '@firebase/app';
import '@firebase/auth';
import { LoginwalasPage } from '../loginwalas/loginwalas.page';

@Component({
  selector: 'app-daftar',
  templateUrl: './daftar.page.html',
  styleUrls: ['./daftar.page.scss'],
})
export class DaftarPage implements OnInit {

  email: string = "";
  password: string = "";

  constructor(
    public alert: AlertController,
    public modalController : ModalController,
    public router: Router,
  ) { }

  ngOnInit() {
  }
  async loginwalas()
  {
    const modal = await this.modalController.create({
      component: LoginwalasPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  async daftar()
  {
    firebase.auth().createUserWithEmailAndPassword(this.email, this.password).then(
      (data)=>{
        this.showAlert("Daftar Berhasil", "Daftar Berhasil");
        console.log("Daftar Berhasil");
      }).catch((err)=>{
        this.showAlert("Error", err.message)
        console.log(err)
      })
  }
  async showAlert(header:string, message:string){
    const alert = await this.alert.create({
      header,
      message,
      buttons: ["OK"]
    })
    await alert.present()
  }

}
