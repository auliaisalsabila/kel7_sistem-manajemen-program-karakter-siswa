import { Component, OnInit } from '@angular/core';
import { LoginbkPage } from '../loginbk/loginbk.page';
import { ModalController } from '@ionic/angular';
import {Router} from '@angular/router';
import { Kelas11bkPage } from '../kelas11bk/kelas11bk.page';
import { Kelas12bkPage } from '../kelas12bk/kelas12bk.page';
import { Kelas10bkPage } from '../kelas10bk/kelas10bk.page';

@Component({
  selector: 'app-menubk1',
  templateUrl: './menubk1.page.html',
  styleUrls: ['./menubk1.page.scss'],
})
export class Menubk1Page implements OnInit {

  constructor(
    public modalController : ModalController,
    public router: Router,
  ) { }

  ngOnInit() {
  }

  async loginbk()
  {
    const modal = await this.modalController.create({
      component: LoginbkPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async kelas10bk()
  {
    const modal = await this.modalController.create({
      component: Kelas10bkPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async kelas11bk()
  {
    const modal = await this.modalController.create({
      component: Kelas11bkPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async kelas12bk()
  {
    const modal = await this.modalController.create({
      component: Kelas12bkPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
