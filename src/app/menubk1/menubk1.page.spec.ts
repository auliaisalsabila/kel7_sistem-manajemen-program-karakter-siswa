import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Menubk1Page } from './menubk1.page';

describe('Menubk1Page', () => {
  let component: Menubk1Page;
  let fixture: ComponentFixture<Menubk1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menubk1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Menubk1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
