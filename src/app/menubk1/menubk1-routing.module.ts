import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Menubk1Page } from './menubk1.page';

const routes: Routes = [
  {
    path: '',
    component: Menubk1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Menubk1PageRoutingModule {}
