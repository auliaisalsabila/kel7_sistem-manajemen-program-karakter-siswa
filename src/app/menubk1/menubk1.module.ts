import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Menubk1PageRoutingModule } from './menubk1-routing.module';

import { Menubk1Page } from './menubk1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Menubk1PageRoutingModule
  ],
  declarations: [Menubk1Page]
})
export class Menubk1PageModule {}
