import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UploadbkPage } from './uploadbk.page';

const routes: Routes = [
  {
    path: '',
    component: UploadbkPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UploadbkPageRoutingModule {}
