import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import {Router} from '@angular/router';
import { Menuwalas1Page } from '../menuwalas1/menuwalas1.page';

@Component({
  selector: 'app-uploadbk',
  templateUrl: './uploadbk.page.html',
  styleUrls: ['./uploadbk.page.scss'],
})
export class UploadbkPage implements OnInit {

  constructor(
    public modalController : ModalController,
    public router: Router,
  ) { }

  ngOnInit() {
  }
  async menuwalas1()
  {
    const modal = await this.modalController.create({
      component: Menuwalas1Page,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
