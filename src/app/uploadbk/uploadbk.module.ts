import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UploadbkPageRoutingModule } from './uploadbk-routing.module';

import { UploadbkPage } from './uploadbk.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UploadbkPageRoutingModule
  ],
  declarations: [UploadbkPage]
})
export class UploadbkPageModule {}
