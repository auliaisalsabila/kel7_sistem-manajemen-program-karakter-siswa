import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UploadbkPage } from './uploadbk.page';

describe('UploadbkPage', () => {
  let component: UploadbkPage;
  let fixture: ComponentFixture<UploadbkPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadbkPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UploadbkPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
