import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Kelas12kepsekPage } from './kelas12kepsek.page';

const routes: Routes = [
  {
    path: '',
    component: Kelas12kepsekPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Kelas12kepsekPageRoutingModule {}
