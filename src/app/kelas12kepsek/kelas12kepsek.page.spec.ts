import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Kelas12kepsekPage } from './kelas12kepsek.page';

describe('Kelas12kepsekPage', () => {
  let component: Kelas12kepsekPage;
  let fixture: ComponentFixture<Kelas12kepsekPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Kelas12kepsekPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Kelas12kepsekPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
