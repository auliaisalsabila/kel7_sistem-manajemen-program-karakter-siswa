import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Kelas12kepsekPageRoutingModule } from './kelas12kepsek-routing.module';

import { Kelas12kepsekPage } from './kelas12kepsek.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Kelas12kepsekPageRoutingModule
  ],
  declarations: [Kelas12kepsekPage]
})
export class Kelas12kepsekPageModule {}
