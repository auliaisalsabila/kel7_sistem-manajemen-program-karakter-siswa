import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { HalamanAwalPage } from '../halaman-awal/halaman-awal.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(
    public modalController : ModalController,
  ) {}

  ngOnInit() {

  }
  selectedSegment:any = 'home';

  async halamanawal()
  {
    const modal = await this.modalController.create({
      component: HalamanAwalPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

}
