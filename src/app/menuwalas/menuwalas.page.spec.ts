import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MenuwalasPage } from './menuwalas.page';

describe('MenuwalasPage', () => {
  let component: MenuwalasPage;
  let fixture: ComponentFixture<MenuwalasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuwalasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MenuwalasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
