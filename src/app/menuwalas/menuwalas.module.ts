import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenuwalasPageRoutingModule } from './menuwalas-routing.module';

import { MenuwalasPage } from './menuwalas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenuwalasPageRoutingModule
  ],
  declarations: [MenuwalasPage]
})
export class MenuwalasPageModule {}
