import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuwalasPage } from './menuwalas.page';

const routes: Routes = [
  {
    path: '',
    component: MenuwalasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuwalasPageRoutingModule {}
