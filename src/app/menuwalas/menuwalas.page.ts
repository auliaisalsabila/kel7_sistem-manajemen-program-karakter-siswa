import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import {Router} from '@angular/router';
import { LoginwalasPage } from '../loginwalas/loginwalas.page';
import { LogoutPage } from '../logout/logout.page';
import { NotifwalasPage } from '../notifwalas/notifwalas.page';
@Component({
  selector: 'app-menuwalas',
  templateUrl: './menuwalas.page.html',
  styleUrls: ['./menuwalas.page.scss'],
})
export class MenuwalasPage implements OnInit {

  constructor(
    public modalController : ModalController,
    public router: Router,
  ) { }

  ngOnInit() {
  }

  async loginwalas()
  {
    const modal = await this.modalController.create({
      component: LoginwalasPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async logout()
  {
    const modal = await this.modalController.create({
      component: LogoutPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async notif()
  {
    const modal = await this.modalController.create({
      component: NotifwalasPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
