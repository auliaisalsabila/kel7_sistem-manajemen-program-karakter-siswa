import { Component, OnInit } from '@angular/core';
import { Menubk1Page } from '../menubk1/menubk1.page';
import { ModalController } from '@ionic/angular';
import {Router} from '@angular/router';

@Component({
  selector: 'app-kelas12bk',
  templateUrl: './kelas12bk.page.html',
  styleUrls: ['./kelas12bk.page.scss'],
})
export class Kelas12bkPage implements OnInit {

  constructor(
    public modalController : ModalController,
    public router: Router,
  ) { }

  ngOnInit() {
  }
  async menubk1()
  {
    const modal = await this.modalController.create({
      component: Menubk1Page,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

}
