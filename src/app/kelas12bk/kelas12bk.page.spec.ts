import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Kelas12bkPage } from './kelas12bk.page';

describe('Kelas12bkPage', () => {
  let component: Kelas12bkPage;
  let fixture: ComponentFixture<Kelas12bkPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Kelas12bkPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Kelas12bkPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
