import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Kelas12bkPageRoutingModule } from './kelas12bk-routing.module';

import { Kelas12bkPage } from './kelas12bk.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Kelas12bkPageRoutingModule
  ],
  declarations: [Kelas12bkPage]
})
export class Kelas12bkPageModule {}
