import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Uploadkelas12PageRoutingModule } from './uploadkelas12-routing.module';

import { Uploadkelas12Page } from './uploadkelas12.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Uploadkelas12PageRoutingModule
  ],
  declarations: [Uploadkelas12Page]
})
export class Uploadkelas12PageModule {}
