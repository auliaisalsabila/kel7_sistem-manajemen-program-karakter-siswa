import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Uploadkelas12Page } from './uploadkelas12.page';

describe('Uploadkelas12Page', () => {
  let component: Uploadkelas12Page;
  let fixture: ComponentFixture<Uploadkelas12Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Uploadkelas12Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Uploadkelas12Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
