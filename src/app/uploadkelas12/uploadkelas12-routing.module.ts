import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Uploadkelas12Page } from './uploadkelas12.page';

const routes: Routes = [
  {
    path: '',
    component: Uploadkelas12Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Uploadkelas12PageRoutingModule {}
