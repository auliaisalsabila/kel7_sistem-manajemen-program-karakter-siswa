import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotifsiswaPageRoutingModule } from './notifsiswa-routing.module';

import { NotifsiswaPage } from './notifsiswa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotifsiswaPageRoutingModule
  ],
  declarations: [NotifsiswaPage]
})
export class NotifsiswaPageModule {}
