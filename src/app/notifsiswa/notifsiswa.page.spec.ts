import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NotifsiswaPage } from './notifsiswa.page';

describe('NotifsiswaPage', () => {
  let component: NotifsiswaPage;
  let fixture: ComponentFixture<NotifsiswaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotifsiswaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NotifsiswaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
