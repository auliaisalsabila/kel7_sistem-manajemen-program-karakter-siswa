import { Component, OnInit } from '@angular/core';
import { MenusiswaPage } from '../menusiswa/menusiswa.page';
import { ModalController } from '@ionic/angular';
import {Router} from '@angular/router';


@Component({
  selector: 'app-notifsiswa',
  templateUrl: './notifsiswa.page.html',
  styleUrls: ['./notifsiswa.page.scss'],
})
export class NotifsiswaPage implements OnInit {

  constructor(
    public modalController : ModalController,
    public router: Router,
  ) { }

  ngOnInit() {
  }
  
  async menusiswa()
  {
    const modal = await this.modalController.create({
      component: MenusiswaPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

}
