import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { HalamanAwalPage } from '../halaman-awal/halaman-awal.page';
import {Router} from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';

import firebase from '@firebase/app';
import '@firebase/auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  email: string = "";
  password: string = "";
  afAuth: any;
  db: any;

  constructor(
    public modalController : ModalController,
    public router: Router,
    public alert: AlertController,
    public auth: AngularFireAuth,
  ) { }

  ngOnInit() {
    this.checkLogin();
  }

  selectedSegment:any = 'home';

  async halamanawal()
  {
    const modal = await this.modalController.create({
      component: HalamanAwalPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  user: any = {};
  login()
  {
    firebase.auth().signInWithEmailAndPassword(this.email, this.password).then((data) =>
    {
      this.modalController.dismiss();
      this.router.navigate(['menukepsek1']);
    }).catch((err)=>{
      this.showAlert("Login Gagal", err.message)
      console.log("Login Gagal")
    })
  }
  async showAlert(header:string, message:string){
    const alert = await this.alert.create({
      header,
      message,
      buttons: ["OK"]
    })
    await alert.present()
  }
  login1(email,password) {
    this.afAuth.auth.signInWithEmailAndPassword(email, password).then(res=>{
      this.cekAkses(res.user.uid);
    }).catch(err=> {
      this.showAlert("Login Gagal", err.message)
      console.log("Login Gagal")
    })
  }
  cekAkses(uid: any){
    this.db.collection('users').doc(uid).get().subscribe(res => {
      if (res.data() != undefined ) {
        this.routerAction(res.data());
      } else this.noacces();
    })
  }
  routerAction(data) {
    if (data.role == 'user') {
      localStorage.setItem('uid', data.uid);
      this.dismiss();
      this.router.navigate(['/menukepsek1']);
    } else this.noacces();
  }
  noacces() {
    this.afAuth.auth.signOut();
    alert('Tidak ada akses');
  }
  dismiss() {
    throw new Error('Method not implemented.');
  }
  checkLogin()
  {
    this.auth.onAuthStateChanged(user=>{
      if(user.uid != null) this.router.navigate(['/menukepsek1']);
      return;
    })
  }
}
