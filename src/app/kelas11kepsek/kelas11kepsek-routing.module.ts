import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Kelas11kepsekPage } from './kelas11kepsek.page';

const routes: Routes = [
  {
    path: '',
    component: Kelas11kepsekPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Kelas11kepsekPageRoutingModule {}
