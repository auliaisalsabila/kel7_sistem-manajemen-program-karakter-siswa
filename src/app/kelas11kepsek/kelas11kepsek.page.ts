import { ModalController } from '@ionic/angular';
import {Router} from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Menukepsek1Page } from '../menukepsek1/menukepsek1.page';
import { LogoutPage } from '../logout/logout.page';

@Component({
  selector: 'app-kelas11kepsek',
  templateUrl: './kelas11kepsek.page.html',
  styleUrls: ['./kelas11kepsek.page.scss'],
})
export class Kelas11kepsekPage implements OnInit {

  constructor(
    public modalController : ModalController,
    public router: Router,
  ) { }

  ngOnInit() {
  }

  async menukepsek1()
  {
    const modal = await this.modalController.create({
      component: Menukepsek1Page,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async logout()
  {
    const modal = await this.modalController.create({
      component: LogoutPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
