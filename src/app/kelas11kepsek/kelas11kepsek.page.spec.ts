import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Kelas11kepsekPage } from './kelas11kepsek.page';

describe('Kelas11kepsekPage', () => {
  let component: Kelas11kepsekPage;
  let fixture: ComponentFixture<Kelas11kepsekPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Kelas11kepsekPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Kelas11kepsekPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
