import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Kelas11kepsekPageRoutingModule } from './kelas11kepsek-routing.module';

import { Kelas11kepsekPage } from './kelas11kepsek.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Kelas11kepsekPageRoutingModule
  ],
  declarations: [Kelas11kepsekPage]
})
export class Kelas11kepsekPageModule {}
