import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Menuwalas1Page } from './menuwalas1.page';

const routes: Routes = [
  {
    path: '',
    component: Menuwalas1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Menuwalas1PageRoutingModule {}
