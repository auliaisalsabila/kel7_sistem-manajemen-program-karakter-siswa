import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Menuwalas1Page } from './menuwalas1.page';

describe('Menuwalas1Page', () => {
  let component: Menuwalas1Page;
  let fixture: ComponentFixture<Menuwalas1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menuwalas1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Menuwalas1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
