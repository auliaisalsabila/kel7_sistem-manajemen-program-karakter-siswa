import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Menuwalas1PageRoutingModule } from './menuwalas1-routing.module';

import { Menuwalas1Page } from './menuwalas1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Menuwalas1PageRoutingModule
  ],
  declarations: [Menuwalas1Page]
})
export class Menuwalas1PageModule {}
