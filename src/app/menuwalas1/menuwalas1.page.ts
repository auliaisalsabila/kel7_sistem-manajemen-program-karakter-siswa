import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import {Router} from '@angular/router';
import { MenuwalasPage } from '../menuwalas/menuwalas.page';
import { Uploadkelas10Page } from '../uploadkelas10/uploadkelas10.page';
import { Uploadkelas11Page } from '../uploadkelas11/uploadkelas11.page';
import { Uploadkelas12Page } from '../uploadkelas12/uploadkelas12.page';
import { UploadkepsekPage } from '../uploadkepsek/uploadkepsek.page';
import { UploadbkPage } from '../uploadbk/uploadbk.page';
import { UploadwaliPage } from '../uploadwali/uploadwali.page';

@Component({
  selector: 'app-menuwalas1',
  templateUrl: './menuwalas1.page.html',
  styleUrls: ['./menuwalas1.page.scss'],
})
export class Menuwalas1Page implements OnInit {

  constructor(
    public modalController : ModalController,
    public router: Router,
  ) { }

  ngOnInit() {
  }

  async menuwalas()
  {
    const modal = await this.modalController.create({
      component: MenuwalasPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async upload10()
  {
    const modal = await this.modalController.create({
      component: Uploadkelas10Page,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async upload11()
  {
    const modal = await this.modalController.create({
      component: Uploadkelas11Page,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async upload12()
  {
    const modal = await this.modalController.create({
      component: Uploadkelas12Page,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async uploadkepsek()
  {
    const modal = await this.modalController.create({
      component: UploadkepsekPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async uploadbk()
  {
    const modal = await this.modalController.create({
      component: UploadbkPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async uploadwali()
  {
    const modal = await this.modalController.create({
      component: UploadwaliPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
