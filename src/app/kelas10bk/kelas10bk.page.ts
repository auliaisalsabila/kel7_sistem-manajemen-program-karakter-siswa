import { Component, OnInit } from '@angular/core';
import { Menubk1Page } from '../menubk1/menubk1.page';
import { ModalController } from '@ionic/angular';
import {Router} from '@angular/router';

@Component({
  selector: 'app-kelas10bk',
  templateUrl: './kelas10bk.page.html',
  styleUrls: ['./kelas10bk.page.scss'],
})
export class Kelas10bkPage implements OnInit {

  constructor(
    public modalController : ModalController,
    public router: Router,
  ) { }

  ngOnInit() {
  }

  async menubk1()
  {
    const modal = await this.modalController.create({
      component: Menubk1Page,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

}
