import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Kelas10bkPage } from './kelas10bk.page';

const routes: Routes = [
  {
    path: '',
    component: Kelas10bkPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Kelas10bkPageRoutingModule {}
