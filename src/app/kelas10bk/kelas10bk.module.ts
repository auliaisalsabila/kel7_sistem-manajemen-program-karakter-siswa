import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Kelas10bkPageRoutingModule } from './kelas10bk-routing.module';

import { Kelas10bkPage } from './kelas10bk.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Kelas10bkPageRoutingModule
  ],
  declarations: [Kelas10bkPage]
})
export class Kelas10bkPageModule {}
