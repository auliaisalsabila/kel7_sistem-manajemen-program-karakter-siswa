import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Kelas10bkPage } from './kelas10bk.page';

describe('Kelas10bkPage', () => {
  let component: Kelas10bkPage;
  let fixture: ComponentFixture<Kelas10bkPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Kelas10bkPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Kelas10bkPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
