import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Uploadkelas10Page } from './uploadkelas10.page';

describe('Uploadkelas10Page', () => {
  let component: Uploadkelas10Page;
  let fixture: ComponentFixture<Uploadkelas10Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Uploadkelas10Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Uploadkelas10Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
