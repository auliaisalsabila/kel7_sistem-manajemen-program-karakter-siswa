import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Uploadkelas10Page } from './uploadkelas10.page';

const routes: Routes = [
  {
    path: '',
    component: Uploadkelas10Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Uploadkelas10PageRoutingModule {}
