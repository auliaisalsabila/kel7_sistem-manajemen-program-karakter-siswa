import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Uploadkelas10PageRoutingModule } from './uploadkelas10-routing.module';

import { Uploadkelas10Page } from './uploadkelas10.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Uploadkelas10PageRoutingModule
  ],
  declarations: [Uploadkelas10Page]
})
export class Uploadkelas10PageModule {}
