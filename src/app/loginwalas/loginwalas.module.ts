import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginwalasPageRoutingModule } from './loginwalas-routing.module';

import { LoginwalasPage } from './loginwalas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoginwalasPageRoutingModule
  ],
  declarations: [LoginwalasPage]
})
export class LoginwalasPageModule {}
