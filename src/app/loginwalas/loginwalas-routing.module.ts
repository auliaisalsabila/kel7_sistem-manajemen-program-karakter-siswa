import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginwalasPage } from './loginwalas.page';

const routes: Routes = [
  {
    path: '',
    component: LoginwalasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginwalasPageRoutingModule {}
