import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LoginwalasPage } from './loginwalas.page';

describe('LoginwalasPage', () => {
  let component: LoginwalasPage;
  let fixture: ComponentFixture<LoginwalasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginwalasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LoginwalasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
