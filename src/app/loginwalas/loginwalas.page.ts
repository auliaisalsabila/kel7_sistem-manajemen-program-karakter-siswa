import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { HalamanAwalPage } from '../halaman-awal/halaman-awal.page';
import {Router} from '@angular/router';
import { DaftarPage } from '../daftar/daftar.page';
import { AlertController } from '@ionic/angular';

import firebase from '@firebase/app';
import '@firebase/auth';

@Component({
  selector: 'app-loginwalas',
  templateUrl: './loginwalas.page.html',
  styleUrls: ['./loginwalas.page.scss'],
})
export class LoginwalasPage implements OnInit {

  email: string = "";
  password: string = "";

  constructor(
    public modalController : ModalController,
    public router: Router,
    public alert: AlertController,
  ) { }

  ngOnInit() {
  }
  async halamanawal()
  {
    const modal = await this.modalController.create({
      component: HalamanAwalPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async daftar()
  {
    const modal = await this.modalController.create({
      component: DaftarPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async loginwalas()
  {
    firebase.auth().signInWithEmailAndPassword(this.email, this.password).then((data) =>
    {
      this.modalController.dismiss();
      this.router.navigate(['menuwalas']);
    }).catch((err)=>{
      this.showAlert("Login Gagal", err.message)
      console.log("Login Gagal")
    })
  }
  async showAlert(header:string, message:string){
    const alert = await this.alert.create({
      header,
      message,
      buttons: ["OK"]
    })
    await alert.present()
  }
}
